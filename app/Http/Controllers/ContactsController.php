<?php

namespace App\Http\Controllers;

use App\Models\Contacts;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all contacts
        $contacts = Contacts::all();

        //return contacts as a resource for the view
        return view('contacts.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contacts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate the request
        $request->validate([
            'name' => 'required|min:5', //minimum of 5 characters
            'email' => 'required|max:255|email|unique:contacts,email', //unique and valid email
            'contact' => 'required|digits:9|unique:contacts,contact'//accept only 9 digits
        ]);

        Contacts::create($request->all());

        return redirect()->route('contacts.index')
            ->with('success','Contact created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contacts  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contacts $contact)
    {
        //return contact as a resource for the view
        return view('contacts.show', compact('contact'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Contacts  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contacts $contact)
    {
        return view('contacts.edit', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreContactsRequest $request
     * @param Contacts $contact
     * @return Response
     */
    public function update(Request $request, Contacts $contact)
    {
        //validate the request
        $request->validate([
            'name' => 'required|min:5', //minimum of 5 characters
            'email' => 'required|max:255|email|unique:contacts,email,'.$contact->id, //unique and valid email
            'contact' => 'required|digits:9|unique:contacts,contact,'.$contact->id //accept only 9 digits
        ]);

        $contact->update($request->all());

        return redirect()->route('contacts.edit', $contact)
            ->with('success','Contact updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contacts  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contacts $contact)
    {
        $contact->delete();

        return redirect()->route('contacts.index')
            ->with('success','Contact deleted successfully');
    }
}
