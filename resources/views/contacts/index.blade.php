
@extends('layouts.app')

@section('card-header')

    <div class="row">
        <div class="col-sm-6">
            Contacts
        </div>
        <div class="col-sm-6  text-right">
            <a class="btn btn-small btn-primary" href="{{ route('contacts.create') }}">
                Create
            </a>
        </div>
    </div>

@endsection

@section('card-body')

    <table class="table">
        <tr>
            <th>Name</th>
            <th>Actions</th>
        </tr>
        @foreach($contacts as $contact)
            <tr>
                <td>{{ $contact->name }}</td>
                <td class="row">
                    <div class="col-sm-3">
                        <a class="btn btn-small btn-success" href="{{ route('contacts.show', $contact) }}">
                            Details
                        </a>
                    </div>

                    <div class="col-sm-3">
                        <a class="btn btn-small btn-info" href="{{ route('contacts.edit', $contact) }}">
                            Edit
                        </a>
                    </div>


                    <div class="col-sm-3">
                        <form action="{{ route('contacts.destroy', $contact) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger ">Delete</button>
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
    </table>



@endsection
