@extends('layouts.app')

@section('card-header')

    Details

@endsection

@section('card-body')

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>Name</h3>
                <p>{{$contact->name}}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h3>Contact</h3>
                <p>{{$contact->contact}}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h3>Email</h3>
                <p>{{$contact->email}}</p>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-4">
                <a class="btn btn-small btn-info" href="{{ route('contacts.edit', $contact) }}">
                    Edit
                </a>
            </div>
            <div class="col-sm-4">
                <form action="{{ route('contacts.destroy', $contact) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>

@endsection

